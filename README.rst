####################
django-smart-admin
####################

Improved Django admin utilities (display images, easy internal links, ...)


Install
=======================================

Add the following line in your requirements (yes, there is no pip package for now):

.. code-block::
    git+https://gitlab.com/jeremymarc/django-smart-admin@0.1.2#egg=django-smart-admin


NB: choose the version you want by changing ``@0.1.2`` in the URL.


Features
=======================================

* **feature:** description
* **Speed optimizations compatible:** the suite is compatible with ``--parallel`` and ``--keepdb``, and let you disable migrations without loosing PostgreSql extensions.


Todo
=======================================

- Nothing here
