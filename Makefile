# Always execute targets even if nothing has changed
.PHONY: *
# Execute all target commands in the same shell (allow envvar to persist)
.ONESHELL:
# Do not continue upon errors
.SHELLFLAGS = -ec

# Colors
C_BLACK     	= \\033[0;30m
C_RED       	= \\033[1;31m
C_GREEN    	 	= \\033[0;32m
C_YELLOW    	= \\033[0;33m
C_BLUE      	= \\033[1;34m
C_PURPLE    	= \\033[0;35m
C_CYAN      	= \\033[0;36m
C_WHITE     	= \\033[0;37m
C_END			= \\033[0m
C_BOLD	 		= \\033[1;37m

# Defaults
ECHO 			= echo
SED         	= sed
PYTHON			= python
RM				= rm
RM_EXTS			= '*~' '*\#' '*.pyc' '*.egg-info'
RM_PATHS		= .coverage coverage.xml htmlcov violations.txt
BRANCH			= $(shell git rev-parse --abbrev-ref HEAD)
RELEASE_BRANCH  = main
VERSION_FILE    = "smart_admin/__init__.py"
GET_VERSION		= $(shell $(SED) -nE 's/^\s*__version__ = "(.*?)"$$/\1/p' $(VERSION_FILE))
GET_LAST_TAG	= $(shell git describe --tags)

# MUST BE THE FIRST TARGET
help:
	@$(ECHO) "Usage: make <command> [VAR=value]*"
	@$(ECHO) "  clean:   		remove build, tmp files..."
	@$(ECHO) "  config:  		display configuration variables"
	@$(ECHO) "  release: 		generate a new release on branch $(C_BOLD)$(RELEASE_BRANCH)$(C_END) (version + tag + release notes)"
	@$(ECHO) "  test:    		run tests"


# MUST NOT BE THE FIRST TARGET
ifndef VERBOSE
# Silent the Makefile. Use VERBOSE to disable.
.SILENT:
endif

clean:
	$(ECHO) "$(C_BLUE)$(RM) -rf $(C_YELLOW)$(RM_PATHS)$(C_END)"
	$(RM) -rf $(RM_PATHS)
	for ext in $(RM_EXTS); \
	do \
		$(ECHO) "$(C_BLUE)find . -type f -name $(C_YELLOW)$$ext $(C_BLUE)-delete$(C_END)"; \
		find . -type f -name "$$ext" -delete; \
	done

config:
	$(ECHO) "$(C_BLUE)SERVER VERSION\t$(C_YELLOW)$(GET_VERSION)$(C_END)"
	$(ECHO) "$(C_BLUE)GIT BRANCH\t$(C_YELLOW)$(BRANCH)@$(GET_LAST_TAG)$(C_END)"
	$(ECHO) "$(C_BLUE)PYTHON EXE\t$(C_YELLOW)$(PYTHON)$(C_END)"
	$(ECHO) "$(C_BLUE)PYTHON VERSION\t$(C_YELLOW)$(shell $(PYTHON) --version)$(C_END)"

confirm:
	$(ECHO) "$(MSG)\nEnter $(C_BOLD)$(PASS)$(C_END) to confirm"
	read -p "> " ANSWER; \
	case "$$ANSWER" in \
		$(PASS) ) exit 0;; \
		* ) $(ECHO) "$(C_RED)CANCELLED$(C_END)"; exit 1;; \
	esac

##### TESTS #####

test:
	$(ECHO) "$(C_GREEN)Success$(C_END)"

##### DEPLOYMENTS #####

allow_release:
# Checking release type
ifndef TYPE
	$(ECHO) "$(C_RED)You must specify a release type $(C_YELLOW)major $(C_RED)|$(C_YELLOW) minor $(C_RED)|$(C_YELLOW) patch$(C_RED) (example: $(C_BLUE)make release TYPE=patch$(C_RED))$(C_END)"
	exit 1
endif
# Checking branch
ifneq ($(BRANCH), $(RELEASE_BRANCH))
	$(ECHO) "$(C_RED)You need to be on $(C_YELLOW)$(RELEASE_BRANCH)$(C_RED) to release$(C_END)"
	exit 1
endif
# Checking git
# Thanks to https://stackoverflow.com/a/57510633
	git update-index -q --ignore-submodules --refresh
ifneq ($(shell git diff-files --quiet --ignore-submodules; $(ECHO) $$?), 0)
	$(ECHO) "$(C_RED)Your git repository is dirty, please commit/stash/checkout before release$(C_END)"
	exit 1
endif
ifneq ($(shell git diff-index --cached --quiet --ignore-submodules HEAD -- 2>&1 > /dev/null; $(ECHO) $$?), 0)
	$(ECHO) "$(C_RED)Your git repository is outdated, please pull before release$(C_END)"
	exit 1
endif

release: allow_release test
# Thanks to and https://stackoverflow.com/a/54776239 and https://stackoverflow.com/a/1909390 and https://unix.stackexchange.com/a/575097
	VERSION=$(shell ./inc_version.sh $(TYPE) $(VERSION_FILE))
	$(ECHO) "$(C_RED)Generating version $(C_YELLOW)$$VERSION$(C_RED) on branch $(C_YELLOW)$(BRANCH)$(C_END)"
	git commit -am "Makefile: creating release $$VERSION"
	git tag -fa "$$VERSION"
	git push origin
	git push origin "$$VERSION"
