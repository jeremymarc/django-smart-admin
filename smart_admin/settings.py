"""
Settings for django-smart-admon are all namespaced in the SMART_ADMIN setting.
For example your project's `settings.py` file might look like this:

SMART_ADMIN = {
    'ATTR': <value>,
}

This module provides the `admin_settings` object, that is used to access
django-smart-admin settings, checking for user settings first, then falling
back to the defaults.

Inspired by rest_framework settings:
https://github.com/encode/django-rest-framework/blob/master/rest_framework/settings.py
"""
from typing import List
import logging

from django.conf import settings
from django.test.signals import setting_changed

logger = logging.getLogger("smart_admin")

DEFAULTS = {}

REMOVED_SETTINGS: List[str] = []


class Settings:
    """
    A settings object that allows SMART_ADMIN settings to be accessed as
    properties. For example:

        from smart_admin.settings import admin_settings
        print(admin_settings.<ATTR>)

    Note:
    This is an internal class that is only compatible with settings namespaced
    under the SMART_ADMIN name. It is not intended to be used by 3rd-party
    apps, and test helpers like `override_settings` may not work as expected.
    """

    def __init__(self, user_settings=None, defaults=None):
        if user_settings:
            self._user_settings = self.__check_user_settings(user_settings)
        self.defaults = defaults or DEFAULTS
        self._cached_attrs = set()

    @property
    def user_settings(self):
        if not hasattr(self, "_user_settings"):
            self._user_settings = getattr(settings, "SMART_ADMIN", {})
        return self._user_settings

    def __getattr__(self, attr):
        if attr not in self.defaults:
            raise AttributeError("Invalid SMART_ADMIN setting: '%s'" % attr)

        try:
            # Check if present in user settings
            val = self.user_settings[attr]
        except KeyError:
            # Fall back to defaults
            val = self.defaults[attr]

        # Cache the result
        self._cached_attrs.add(attr)
        setattr(self, attr, val)
        return val

    def __check_user_settings(self, user_settings):
        SETTINGS_DOC = "https://gitlab.com/jeremymarc/django-smart-admin/docs/"
        for setting in REMOVED_SETTINGS:
            if setting in user_settings:
                raise RuntimeError("The '%s' setting has been removed. Please refer to '%s' for available settings." % (setting, SETTINGS_DOC))
        return user_settings

    def reload(self):
        for attr in self._cached_attrs:
            delattr(self, attr)
        self._cached_attrs.clear()
        if hasattr(self, "_user_settings"):
            delattr(self, "_user_settings")

    def log(self, *args, **kwargs):
        logger.debug(*args, **kwargs)


admin_settings = Settings(None, DEFAULTS)


def reload_settings(*args, **kwargs):
    setting = kwargs["setting"]
    if setting == "SMART_ADMIN":
        admin_settings.reload()


setting_changed.connect(reload_settings)
