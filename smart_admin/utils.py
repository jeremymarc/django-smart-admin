import logging
import magicattr

from django.urls import reverse
from django.utils.safestring import mark_safe

logger = logging.getLogger(__name__)


def deep_getattr(obj, attrs, default=None):
    try:
        return magicattr.get(obj, attrs)
    except AttributeError:
        return default


def model_attr(attr, description=None, label=None, order_field=None, default="--", colors=None):
    """
    - attr: string representing an attribute (or a sub attribute instance.<attr.subattr>) or a getter (set it to None to use self)
    - description: column title in admin
    - order_field: used to order the queryset (set it to "" to disable)
    - default: default value if None
    """
    if attr is None:
        attr_getter = lambda obj: obj
    elif callable(attr):
        attr_getter = attr
    else:
        attr_getter = lambda obj: deep_getattr(obj, attr)
    if label is None:
        label_getter = lambda obj: str(attr_getter(obj))
    elif isinstance(label, str):
        label_getter = lambda obj: deep_getattr(obj, label)
    else:
        assert callable(label)
        label_getter = label
    if callable(colors):
        color_getter = colors
    elif isinstance(colors, dict):
        color_getter = lambda value: colors.get(value, colors.get(None, "black"))
    elif isinstance(colors, list):
        color_getter = lambda value: next(c for v, c in colors if v is None or value <= v)
    else:
        color_getter = None

    def attr_func(self, obj):
        try:
            value = attr_getter(obj)
            if value is None:
                return default
            label = label_getter(obj) or str(value)
            if color_getter:
                color = color_getter(value)
                label = mark_safe('<span style="color:%s;">%s</span>' % (color, label))
            return label
        except Exception as e:
            logger.exception(e)
            return "Error"

    if description is None and not callable(attr):
        description = attr.replace(".", " ").replace("_", " ").capitalize()
    if order_field is None and isinstance(attr, str) and "." not in attr:
        order_field = attr.replace(".", "__")

    # Special function attributes
    attr_func.short_description = description
    if order_field:
        attr_func.admin_order_field = order_field
    return attr_func


def model_link(attr, model, description=None, label=None, default="--", order_field=None):
    """
    Create an HTML link toward the targeted @model admin. This requires that @model already has a registered admin.

    Warning: to prevent performance issues, you need to select or prefetch the related fields.

    Usage:
        class MyAdmin(Admin):
            list_fields = [
                "id",
                "my_field",
                ...
            ]

            my_field = model_link("attr.subattr.final_attr", FinalModel)
    """
    if attr is None:
        getter = lambda obj: obj
    elif callable(attr):
        getter = attr
    else:
        getter = lambda obj: deep_getattr(obj, attr)
    if label is None:
        label_func = lambda obj: str(getter(obj))
    elif isinstance(label, str):
        label_func = lambda obj: deep_getattr(obj, label)
    else:
        label_func = label
    if description is None:
        description = model._meta.model_name
    base_url = "admin:%s_change" % model._meta.db_table

    def link_func(self, obj, default=default):
        if getter(obj) is None:
            return default
        obj_label = label_func(obj) if callable(label_func) else label
        return mark_safe('<a href="%s">%s</a>' % (reverse(base_url, args=(getter(obj).pk,)), obj_label))

    link_func.short_description = description
    if order_field:
        link_func.admin_order_field = order_field
    return link_func


def filtered_list_link(model, query, label=None, count=None):
    target = reverse("admin:" + model + "_changelist") + "?q=" + query
    link = f'<a href="{target}">{label}'
    if count:
        link += f" ({count})"
    link += "</a>"

    return mark_safe(link)


def external_link(url_attr, label_attr=None, description=None, default="--", order_field=None):
    if callable(url_attr):
        url_getter = url_attr
    else:

        def url_getter(obj):
            try:
                url = deep_getattr(obj, url_attr)
                if not url:
                    return None
                if hasattr(url, "url"):
                    # Special case if file/image is given as field without the '.url'
                    return url.url
                return url
            except ValueError:
                # Special case for file/image fields raising exception if no url
                return None

    if callable(label_attr):
        label_getter = label_attr
    else:
        label_getter = lambda obj: deep_getattr(obj, label_attr)

    def link_func(self, obj):
        url = url_getter(obj)
        label = label_getter(obj)
        if label is None:
            label = default
        if not url:
            return default
        return mark_safe('<a href="%s" target="_blank">%s</a>' % (url, label))

    link_func.short_description = description or label_attr or url_attr
    if order_field:
        link_func.admin_order_field = order_field
    return link_func


def image_link(src_attr, url_attr=None, description=None, default="", width=150, order_field=None):
    url_attr = url_attr or src_attr

    if callable(src_attr):
        src_getter = src_attr
    else:
        src_getter = lambda obj: deep_getattr(obj, src_attr, None)

    def label_getter(obj):
        src = src_getter(obj)
        if not src:
            return default
        if hasattr(src, "url"):
            # Special case if file/image is given as field without the '.url'
            src = src.url
        if src.startswith("/") or src.startswith("http"):
            alt = url_attr
        else:
            if src.endswith(".url"):
                alt = deep_getattr(obj, src.replace(".url", ".name"), "")
            else:
                alt = deep_getattr(obj, src + ".name", url_attr)
        return f'<img width="{width}" src="{src}" alt="{alt}" />'

    return external_link(url_attr, label_attr=label_getter, description=description, default=default, order_field=order_field)


def admin_url(db_table=None, pk=None, instance=None):
    if not db_table:
        assert instance is not None, "admin_url needs a model instance or a db_table"
        db_table = instance.__class__._meta.db_table
    if not pk:
        assert instance is not None, "admin_url needs a model instance or a pk"
        pk = instance.pk
    url = reverse(f"admin:{db_table}_change", args=[pk])
    return url
