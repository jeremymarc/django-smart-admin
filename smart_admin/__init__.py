__title__ = "django-smart-admin"
__version__ = "0.1.1"
__license__ = "MIT"
__copyright__ = "Copyright 2024"

# Version synonym
VERSION = __version__
