import logging

from django.db import models
from django.contrib import admin

logger = logging.getLogger(__name__)


class RawInputMixin:
    fk_select_fields: tuple = tuple()
    list_prefetch_related: tuple = tuple()
    list_select_related: tuple = tuple()
    list_annotations: tuple = tuple()

    def formfield_for_dbfield(self, db_field, **kwargs):
        field_name = db_field.name
        if field_name not in self.raw_id_fields and field_name not in self.fk_select_fields:
            if isinstance(db_field, (models.ForeignKey, models.ManyToManyField)):
                self.raw_id_fields = tuple(self.raw_id_fields) + (field_name,)
        return super(RawInputMixin, self).formfield_for_dbfield(db_field, **kwargs)

    def get_model_manager(self, request):
        return self.model._default_manager

    def get_queryset(self, request):
        """
        Return a QuerySet of all model instances that can be edited by the admin site. This is used by changelist_view.

        Note: this code is copied from django admin source, because we want to be able customize the model manager.
        """
        qs = self.get_model_manager(request)
        ordering = self.get_ordering(request)
        # To fix admin crash "variable not found in subplan target list"
        # See https://stackoverflow.com/questions/71126845/how-to-fix-internalerror-variable-not-found-in-subplan-target-list
        qs = qs.filter(pk__gte=0)
        qs = qs.prefetch_related(*self.list_prefetch_related).select_related(*self.list_select_related)
        if self.list_annotations:
            for annotation in self.list_annotations:
                qs = qs.annotate(**annotation)
        if ordering:
            qs = qs.order_by(*ordering)
        return qs

    def get_object(self, request, object_id, from_field=None):
        # TODO: the parent method is calling get_queryset but seems to loose all prefetch/select related ???
        return super().get_object(request, object_id, from_field=from_field)


class ModelAdmin(RawInputMixin, admin.ModelAdmin):
    """
    This model change the default behavior of django admin forms to use raw_id_fields for all foreign keys.
    All admin models should inherit from this class to prevent performance issues.

    Use this new class attribute to force displaying a FK with a <select> instead of a raw_id_field
    """

    pass


class TabularInline(RawInputMixin, admin.TabularInline):
    """
    This model change the default behavior of django admin forms to use raw_id_fields for all foreign keys.
    All admin models should inherit from this class to prevent performance issues.

    Use this new class attribute to force displaying a FK with a <select> instead of a raw_id_field
    """

    can_delete = False
    show_change_link = True
    show_add_link = False
    fk_name = ""
    ordering = ("-pk",)
    extra = 0

    def has_change_permission(self, request, obj=None):
        return self.show_change_link

    def has_add_permission(self, request, obj=None):
        return self.show_add_link

    def has_delete_permission(self, request, obj=None):
        return self.can_delete
